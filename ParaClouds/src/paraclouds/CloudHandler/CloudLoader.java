/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paraclouds.CloudHandler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import paraclouds.CloudEngine.CloudPoint;

/**
 *
 * @author dan
 */
public class CloudLoader {
    private String DELIMITER = " ";
    private String inputFilePath;
    
    public CloudLoader(String pathToFile){
        this.inputFilePath = pathToFile;
    }
    
    public List<CloudPoint> processInputFile() {
        List<CloudPoint> inputList = new ArrayList<>();
        try{
          File inputF = new File(inputFilePath);
          InputStream inputFS = new FileInputStream(inputF);
          BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));
          // skip the header of the csv
          inputList = br.lines().skip(1).map(mapToItem).collect(Collectors.toList());
          br.close();
        } catch (IOException e) {
          System.out.println("File read error.");
        }
        return inputList;
    }
    
    private Function<String, CloudPoint> mapToItem = (line) -> {
        String[] p = line.split(DELIMITER);
        CloudPoint point = new CloudPoint();
        
        point.setX(Float.parseFloat(p[0])); //<-- this is the first column in the csv file
        point.setY(Float.parseFloat(p[1]));
        point.setZ(Float.parseFloat(p[2]));
        
        return point;
    };
}
