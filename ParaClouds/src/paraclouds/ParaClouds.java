/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paraclouds;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import paraclouds.CloudEngine.*;
import paraclouds.CloudHandler.CloudLoader;

/**
 *
 * @author dan
 */
public class ParaClouds {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String pathToFile = null;
        if (args.length == 0) {
            pathToFile = "TestovaciTXT/cloud_1_full.txt";
        } else {
            pathToFile = args[0];
        }
        
        System.out.println("Loading file... " + pathToFile);
        long startTime = System.currentTimeMillis();
        
        List<CloudPoint> inputPoints = new CloudLoader(pathToFile).processInputFile();
        
        long endTime = System.currentTimeMillis();
        long loadingTime = endTime - startTime;
        System.out.println("Loading file took: " + loadingTime + " ms");
        
        System.out.println("Running serial processing!");
        long serialTime = runSerially(inputPoints);
        System.out.println("Serial processing - All done. Total execution time: " + serialTime + " ms");
        
        System.out.println("\nReloading file...");
        inputPoints = new CloudLoader(pathToFile).processInputFile();
        
        System.out.println("Running parallel stream processing!");
        long parallelTime = runParalelly(inputPoints);
        System.out.println("Parallel stream processing - All done. Total execution time: " + parallelTime + " ms");
        
        //long parallelTime = 0;
        
        System.out.println("\nReloading file...");
        inputPoints = new CloudLoader(pathToFile).processInputFile();
        
        System.out.println("Running concurrency stream processing!");
        long concurrentTime = runConcurrently(inputPoints);
        System.out.println("Concurrent stream processing - All done. Total execution time: " + concurrentTime + " ms");
        
        System.out.println("\nReloading file...");
        inputPoints = new CloudLoader(pathToFile).processInputFile();
        
        System.out.println("Running deep concurrency stream processing!");
        long deepConcurrentTime = runDeeplyConcurrently(inputPoints);
        System.out.println("Deep Concurrent stream processing - All done. Total execution time: " + deepConcurrentTime + " ms");
        
        System.out.println("-------- ALL DONE --------");
        System.out.println("Serial time: " + serialTime + " ms." + " Parallel stream time: " + parallelTime + " ms. " + "Concurrency time: " + concurrentTime + " ms." + " Deep concurrency time: " + deepConcurrentTime + " ms.");
        System.out.println("Serial time delta: " + ((float)serialTime / (float)serialTime) + " ." + " Parallel stream delta: " + ((float)parallelTime / (float)serialTime) + " . " + "Concurrency delta: " + ((float)concurrentTime / (float)serialTime) + " ."+ " Deep Concurrency delta: " + ((float)deepConcurrentTime / (float)serialTime) + " .");
        
    }
    
    public static long runSerially(List<CloudPoint> inputPoints) {
        long startTime = System.currentTimeMillis();
        Cloud cloud = new Cloud(inputPoints, null);
        
        System.out.println("Finding boundaries...");
        cloud.calculateBoundaries();
        
        long endTime = System.currentTimeMillis();
        long boundariesTime = endTime - startTime;
        System.out.println("Finding boundaries took: " + boundariesTime + " ms");
        
        System.out.print(cloud.cloudInfo());
        System.out.println();
        
        System.out.println("Making children and assigning points...");
        /*
        int desiredSize = 10000;
        int children = cloud.countOfChildren();
        int pass = 0;
        int diff = children - desiredSize;
        int prevCount = 0;
        
        while (prevCount > diff) {
            System.out.println("Octree level " + (++pass));
            
            cloud.split();
            prevCount = children;
            children = cloud.countOfChildren();
            diff = children - desiredSize;
            System.out.print(cloud.cloudInfo());
        }
        */
        
        int splits = 7;
        for (int i = 1; i <= splits; i++) {
            //System.out.println("Octree level " + (i));
            cloud.split();
            //System.out.print(cloud.cloudInfo());
            //System.out.println();
        }
        
        endTime = System.currentTimeMillis();
        long childrenMakingTime = endTime - startTime;
        System.out.println("Making children took: " + childrenMakingTime + " ms");
        //System.out.print(cloud.cloudInfo());
        //System.out.println();
        
        System.out.println("Joining points in bottom level children (simplifying)...");
        cloud.simplify();
        
        endTime = System.currentTimeMillis();
        long simplifyingTime = endTime - startTime;
        System.out.println("Simplifying took: " + simplifyingTime + " ms");
        //System.out.print(cloud.cloudInfo());
                
        return (boundariesTime + childrenMakingTime + simplifyingTime);
    }
    
    public static long runParalelly(List<CloudPoint> inputPoints) {
        long startTime = System.currentTimeMillis();
        ParallelStreamCloud cloud = new ParallelStreamCloud(inputPoints, null);
        
        System.out.println("Parallel Stream - Finding boundaries...");
        cloud.calculateBoundaries();
        
        long endTime = System.currentTimeMillis();
        long boundariesTime = endTime - startTime;
        System.out.println("Parallel Stream - Finding boundaries took: " + boundariesTime + " ms");
        
        //System.out.print(cloud.cloudInfo());
        //System.out.println();
        
        System.out.println("Parallel Stream - Making children and assigning points...");
        /*
        int desiredSize = 10000;
        int children = cloud.countOfChildren();
        int pass = 0;
        int diff = children - desiredSize;
        int prevCount = 0;
        
        while (prevCount > diff) {
            System.out.println("Octree level " + (++pass));
            
            cloud.split();
            prevCount = children;
            children = cloud.countOfChildren();
            diff = children - desiredSize;
            System.out.print(cloud.cloudInfo());
        }
        */
        
        int splits = 7;
        for (int i = 1; i <= splits; i++) {
            //System.out.println("Parallel Stream - Octree level " + (i));
            cloud.split();
            //System.out.print(cloud.cloudInfo());
            //System.out.println();
        }
        
        endTime = System.currentTimeMillis();
        long childrenMakingTime = endTime - startTime;
        System.out.println("Parallel Stream - Making children took: " + childrenMakingTime + " ms");
        //System.out.print(cloud.cloudInfo());
        System.out.println();
        
        System.out.println("Parallel Stream - Joining points in bottom level children (simplifying)...");
        cloud.simplify();
        
        endTime = System.currentTimeMillis();
        long simplifyingTime = endTime - startTime;
        System.out.println("Parallel Stream - Simplifying took: " + simplifyingTime + " ms");
        //System.out.print(cloud.cloudInfo());
                
        return (boundariesTime + childrenMakingTime + simplifyingTime);
    }
    
    public static long runConcurrently(List<CloudPoint> inputPoints){
        long startTime = System.currentTimeMillis();
        ConcurrencyCloud cloud = new ConcurrencyCloud(inputPoints, null);
                
        System.out.println("Concurrency - Finding boundaries...");
        cloud.calculateBoundaries();
        
        long endTime = System.currentTimeMillis();
        long boundariesTime = endTime - startTime;
        System.out.println("Concurrency - Finding boundaries took: " + boundariesTime + " ms");
                
        System.out.println("Concurrency - Making children and assigning points...");

        
        int splits = 7;
        for (int i = 1; i <= splits; i++) {
            cloud.split();
        }
        
        endTime = System.currentTimeMillis();
        long childrenMakingTime = endTime - startTime;
        System.out.println("Concurrency - Making children took: " + childrenMakingTime + " ms");
        System.out.println();
        
        System.out.println("Concurrency - Joining points in bottom level children (simplifying)...");
        cloud.simplify();
        
        endTime = System.currentTimeMillis();
        long simplifyingTime = endTime - startTime;
        System.out.println("Concurrency - Simplifying took: " + simplifyingTime + " ms");
        //System.out.println(cloud.cloudInfo());
        return (boundariesTime + childrenMakingTime + simplifyingTime);
    }
    
    public static long runDeeplyConcurrently(List<CloudPoint> inputPoints){
        int availableProcessors = Runtime.getRuntime().availableProcessors();        
        ExecutorService mainExecutor = Executors.newWorkStealingPool();
        long startTime = System.currentTimeMillis();
        DeepConcurrencyCloud cloud = new DeepConcurrencyCloud(inputPoints, null, 0, availableProcessors, mainExecutor);
        System.out.println("Deep Concurrency - Running with " + availableProcessors + " threads.");        
        System.out.println("Deep Concurrency - Finding boundaries...");
        cloud.calculateBoundaries();
        
        long endTime = System.currentTimeMillis();
        long boundariesTime = endTime - startTime;
        System.out.println("Deep Concurrency - Finding boundaries took: " + boundariesTime + " ms");
                
        System.out.println("Deep Concurrency - Making children, assigning to threads and assigning points...");

        int splits = 7;
        for (int i = 1; i <= splits; i++) {
            //System.out.println("Parallel Stream - Octree level " + (i));
            cloud.split();
            //System.out.print(cloud.cloudInfo());
            //System.out.println();
        }
      
        
        endTime = System.currentTimeMillis();
        long childrenMakingTime = endTime - startTime;
        System.out.println("Deep Concurrency - Making children took: " + childrenMakingTime + " ms");
        System.out.println();
        
        System.out.println("Deep Concurrency - Joining points in bottom level children (simplifying)...");
        cloud.simplify();
        
        endTime = System.currentTimeMillis();
        long simplifyingTime = endTime - startTime;
        System.out.println("Deep Concurrency - Simplifying took: " + simplifyingTime + " ms");
        //System.out.println(cloud.cloudInfo());
        return (boundariesTime + childrenMakingTime + simplifyingTime);
    }
}
