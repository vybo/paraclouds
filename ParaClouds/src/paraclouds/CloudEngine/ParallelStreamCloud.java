/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paraclouds.CloudEngine;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.EnumMap;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

import paraclouds.CloudEngine.Cloud.Boundary;
import paraclouds.CloudEngine.Cloud.Children;

/**
 *
 * @author dan
 */
public class ParallelStreamCloud implements Simplifiable {
        
    protected List<CloudPoint> points = new ArrayList<>(); //Collections.synchronizedList(new ArrayList<>());
    protected EnumMap<Boundary, CloudPoint> boundaries = new EnumMap<>(Boundary.class);
    
    protected ParallelStreamCloud parent;
    protected EnumMap<Children, ParallelStreamCloud> children = new EnumMap<>(Children.class);
    
    public ParallelStreamCloud(List<CloudPoint> points, ParallelStreamCloud parent) {
        this.points = points; // Collections.synchronizedList(points);
        this.parent = parent;
    }
    
    public ParallelStreamCloud(ParallelStreamCloud parent, EnumMap<Boundary, CloudPoint> boundaries) {
        this.parent = parent;
        this.boundaries = boundaries;
    }
    
    public void insertPoint(CloudPoint point) {
        synchronized (this.points) {
            this.points.add(point);
        }
    }
    
    protected boolean pointBelongsToChild(CloudPoint point, Children child) {
        return point.X() >= this.children.get(child).boundaries.get(Boundary.LEFT_BOTTOM_FRONT).X() &&
               point.X() <= this.children.get(child).boundaries.get(Boundary.RIGHT_BOTTOM_FRONT).X() &&
               point.Y() >= this.children.get(child).boundaries.get(Boundary.LEFT_TOP_FRONT).Y() &&
               point.Y() <= this.children.get(child).boundaries.get(Boundary.LEFT_BOTTOM_FRONT).Y() &&
               point.Z() >= this.children.get(child).boundaries.get(Boundary.LEFT_TOP_FRONT).Z() &&
               point.Z() <= this.children.get(child).boundaries.get(Boundary.LEFT_TOP_BACK).Z();
    }
    
    public void calculateBoundaries(){
        this.boundaries.clear();
        
        float minX = points.parallelStream().min((CloudPoint p1, CloudPoint p2)->Float.compare(p1.X(), p2.X())).get().X();
        float minY = points.parallelStream().min((CloudPoint p1, CloudPoint p2)->Float.compare(p1.Y(), p2.Y())).get().Y();     
        float minZ = points.parallelStream().min((CloudPoint p1, CloudPoint p2)->Float.compare(p1.Z(), p2.Z())).get().Z();      
        float maxX = points.parallelStream().max((CloudPoint p1, CloudPoint p2)->Float.compare(p1.X(), p2.X())).get().X(); 
        float maxY = points.parallelStream().max((CloudPoint p1, CloudPoint p2)->Float.compare(p1.Y(), p2.Y())).get().Y();
        float maxZ = points.parallelStream().max((CloudPoint p1, CloudPoint p2)->Float.compare(p1.Z(), p2.Z())).get().Z();
        
        boundaries.put(Boundary.LEFT_BOTTOM_FRONT, new CloudPoint(minX, maxY, minZ));
        boundaries.put(Boundary.RIGHT_BOTTOM_FRONT, new CloudPoint(maxX, maxY, minZ));
        boundaries.put(Boundary.LEFT_BOTTOM_BACK, new CloudPoint(minX, maxY, maxZ));
        boundaries.put(Boundary.RIGHT_BOTTOM_BACK, new CloudPoint(maxX, maxY, maxZ));
        
        boundaries.put(Boundary.LEFT_TOP_FRONT, new CloudPoint(minX, minY, minZ));
        boundaries.put(Boundary.RIGHT_TOP_FRONT, new CloudPoint(maxX, minY, minZ));
        boundaries.put(Boundary.LEFT_TOP_BACK, new CloudPoint(minX, minY, maxZ));
        boundaries.put(Boundary.RIGHT_TOP_BACK, new CloudPoint(maxX, minY, maxZ));
    }
    
    public boolean split() {
        if (!this.hasChildren()) {
            float xSplit = (boundaries.get(Boundary.RIGHT_BOTTOM_BACK).X() - boundaries.get(Boundary.LEFT_BOTTOM_BACK).X()) / 2;
            float ySplit = (boundaries.get(Boundary.LEFT_BOTTOM_FRONT).Y() - boundaries.get(Boundary.LEFT_TOP_FRONT).Y()) / 2;
            float zSplit = (boundaries.get(Boundary.RIGHT_BOTTOM_BACK).Z() - boundaries.get(Boundary.RIGHT_BOTTOM_FRONT).Z()) / 2;
            
            EnumMap<Boundary, CloudPoint> firstChildBoundaries = new EnumMap<>(Boundary.class);
            firstChildBoundaries.put(Boundary.LEFT_BOTTOM_FRONT, this.boundaries.get(Boundary.LEFT_BOTTOM_FRONT));
            firstChildBoundaries.put(Boundary.RIGHT_BOTTOM_FRONT, new CloudPoint(this.boundaries.get(Boundary.LEFT_BOTTOM_FRONT).X() + xSplit, this.boundaries.get(Boundary.LEFT_BOTTOM_FRONT).Y(), this.boundaries.get(Boundary.LEFT_BOTTOM_FRONT).Z()));
            firstChildBoundaries.put(Boundary.LEFT_BOTTOM_BACK, new CloudPoint(this.boundaries.get(Boundary.LEFT_BOTTOM_FRONT).X(), this.boundaries.get(Boundary.LEFT_BOTTOM_FRONT).Y(), this.boundaries.get(Boundary.LEFT_BOTTOM_FRONT).Z() + zSplit));
            firstChildBoundaries.put(Boundary.RIGHT_BOTTOM_BACK, new CloudPoint(this.boundaries.get(Boundary.LEFT_BOTTOM_FRONT).X() + xSplit, this.boundaries.get(Boundary.LEFT_BOTTOM_FRONT).Y(), this.boundaries.get(Boundary.LEFT_BOTTOM_FRONT).Z() + zSplit));
            firstChildBoundaries.put(Boundary.LEFT_TOP_FRONT, new CloudPoint(this.boundaries.get(Boundary.LEFT_TOP_FRONT).X(), this.boundaries.get(Boundary.LEFT_TOP_FRONT).Y(), this.boundaries.get(Boundary.LEFT_TOP_FRONT).Z()));
            firstChildBoundaries.put(Boundary.RIGHT_TOP_FRONT, new CloudPoint(this.boundaries.get(Boundary.LEFT_TOP_FRONT).X() + xSplit, this.boundaries.get(Boundary.LEFT_TOP_FRONT).Y() + ySplit, this.boundaries.get(Boundary.LEFT_TOP_FRONT).Z()));
            firstChildBoundaries.put(Boundary.LEFT_TOP_BACK, new CloudPoint(this.boundaries.get(Boundary.LEFT_TOP_FRONT).X(), this.boundaries.get(Boundary.LEFT_TOP_FRONT).Y() + ySplit, this.boundaries.get(Boundary.LEFT_TOP_FRONT).Z() + zSplit));
            firstChildBoundaries.put(Boundary.RIGHT_TOP_BACK, new CloudPoint(this.boundaries.get(Boundary.LEFT_TOP_FRONT).X() + xSplit, this.boundaries.get(Boundary.LEFT_TOP_FRONT).Y() + ySplit, this.boundaries.get(Boundary.LEFT_TOP_FRONT).Z() + zSplit));
            this.children.put(Children.FIRST, new ParallelStreamCloud(this, firstChildBoundaries));
            
            EnumMap<Boundary, CloudPoint> secondChildBoundaries = new EnumMap<>(Boundary.class);
            secondChildBoundaries.put(Boundary.LEFT_BOTTOM_FRONT, firstChildBoundaries.get(Boundary.RIGHT_BOTTOM_FRONT));
            secondChildBoundaries.put(Boundary.RIGHT_BOTTOM_FRONT, this.boundaries.get(Boundary.RIGHT_BOTTOM_FRONT));
            secondChildBoundaries.put(Boundary.LEFT_BOTTOM_BACK, firstChildBoundaries.get(Boundary.RIGHT_BOTTOM_BACK));
            secondChildBoundaries.put(Boundary.RIGHT_BOTTOM_BACK, new CloudPoint(this.boundaries.get(Boundary.RIGHT_BOTTOM_FRONT).X(), this.boundaries.get(Boundary.RIGHT_BOTTOM_FRONT).Y(), this.boundaries.get(Boundary.RIGHT_BOTTOM_FRONT).Z() + zSplit));
            secondChildBoundaries.put(Boundary.LEFT_TOP_FRONT, firstChildBoundaries.get(Boundary.RIGHT_TOP_FRONT));
            secondChildBoundaries.put(Boundary.RIGHT_TOP_FRONT, new CloudPoint(this.boundaries.get(Boundary.RIGHT_TOP_FRONT).X(), this.boundaries.get(Boundary.RIGHT_TOP_FRONT).Y() + ySplit, this.boundaries.get(Boundary.RIGHT_TOP_FRONT).Z()));
            secondChildBoundaries.put(Boundary.LEFT_TOP_BACK, firstChildBoundaries.get(Boundary.RIGHT_TOP_BACK));
            secondChildBoundaries.put(Boundary.RIGHT_TOP_BACK, new CloudPoint(this.boundaries.get(Boundary.RIGHT_TOP_FRONT).X() + xSplit, this.boundaries.get(Boundary.RIGHT_TOP_FRONT).Y() + ySplit, this.boundaries.get(Boundary.RIGHT_TOP_FRONT).Z() + zSplit));
            this.children.put(Children.SECOND, new ParallelStreamCloud(this, secondChildBoundaries));
            
            EnumMap<Boundary, CloudPoint> thirdChildBoundaries = new EnumMap<>(Boundary.class);
            thirdChildBoundaries.put(Boundary.LEFT_BOTTOM_FRONT, firstChildBoundaries.get(Boundary.LEFT_BOTTOM_BACK));
            thirdChildBoundaries.put(Boundary.RIGHT_BOTTOM_FRONT, firstChildBoundaries.get(Boundary.RIGHT_BOTTOM_BACK));
            thirdChildBoundaries.put(Boundary.LEFT_BOTTOM_BACK, this.boundaries.get(Boundary.LEFT_BOTTOM_BACK));
            thirdChildBoundaries.put(Boundary.RIGHT_BOTTOM_BACK, new CloudPoint(this.boundaries.get(Boundary.LEFT_BOTTOM_BACK).X() + xSplit, this.boundaries.get(Boundary.LEFT_BOTTOM_BACK).Y(), this.boundaries.get(Boundary.LEFT_BOTTOM_BACK).Z()));
            thirdChildBoundaries.put(Boundary.LEFT_TOP_FRONT, firstChildBoundaries.get(Boundary.LEFT_TOP_BACK));
            thirdChildBoundaries.put(Boundary.RIGHT_TOP_FRONT, firstChildBoundaries.get(Boundary.RIGHT_TOP_BACK));
            thirdChildBoundaries.put(Boundary.LEFT_TOP_BACK, new CloudPoint(this.boundaries.get(Boundary.LEFT_TOP_BACK).X(), this.boundaries.get(Boundary.LEFT_TOP_BACK).Y() + ySplit, this.boundaries.get(Boundary.LEFT_TOP_BACK).Z()));
            thirdChildBoundaries.put(Boundary.RIGHT_TOP_BACK, new CloudPoint(this.boundaries.get(Boundary.LEFT_TOP_BACK).X() + xSplit, this.boundaries.get(Boundary.LEFT_TOP_BACK).Y() + ySplit, this.boundaries.get(Boundary.LEFT_TOP_BACK).Z()));
            this.children.put(Children.THIRD, new ParallelStreamCloud(this, thirdChildBoundaries));
            
            EnumMap<Boundary, CloudPoint> fourthChildBoundaries = new EnumMap<>(Boundary.class);
            fourthChildBoundaries.put(Boundary.LEFT_BOTTOM_FRONT, firstChildBoundaries.get(Boundary.RIGHT_BOTTOM_BACK));
            fourthChildBoundaries.put(Boundary.RIGHT_BOTTOM_FRONT, secondChildBoundaries.get(Boundary.RIGHT_BOTTOM_BACK));
            fourthChildBoundaries.put(Boundary.LEFT_BOTTOM_BACK, thirdChildBoundaries.get(Boundary.RIGHT_BOTTOM_BACK));
            fourthChildBoundaries.put(Boundary.RIGHT_BOTTOM_BACK, this.boundaries.get(Boundary.RIGHT_BOTTOM_FRONT));
            fourthChildBoundaries.put(Boundary.LEFT_TOP_FRONT, firstChildBoundaries.get(Boundary.RIGHT_TOP_BACK));
            fourthChildBoundaries.put(Boundary.RIGHT_TOP_FRONT, secondChildBoundaries.get(Boundary.RIGHT_TOP_BACK));
            fourthChildBoundaries.put(Boundary.LEFT_TOP_BACK, thirdChildBoundaries.get(Boundary.RIGHT_TOP_BACK));
            fourthChildBoundaries.put(Boundary.RIGHT_TOP_BACK, new CloudPoint(this.boundaries.get(Boundary.RIGHT_TOP_BACK).X(), this.boundaries.get(Boundary.RIGHT_TOP_BACK).Y() + ySplit, this.boundaries.get(Boundary.RIGHT_TOP_BACK).Z()));
            this.children.put(Children.FOURTH, new ParallelStreamCloud(this, fourthChildBoundaries));
            
            EnumMap<Boundary, CloudPoint> fifthChildBoundaries = new EnumMap<>(Boundary.class);
            fifthChildBoundaries.put(Boundary.LEFT_BOTTOM_FRONT, firstChildBoundaries.get(Boundary.LEFT_TOP_FRONT));
            fifthChildBoundaries.put(Boundary.RIGHT_BOTTOM_FRONT, firstChildBoundaries.get(Boundary.RIGHT_TOP_FRONT));
            fifthChildBoundaries.put(Boundary.LEFT_BOTTOM_BACK, firstChildBoundaries.get(Boundary.LEFT_TOP_BACK));
            fifthChildBoundaries.put(Boundary.RIGHT_BOTTOM_BACK, firstChildBoundaries.get(Boundary.RIGHT_TOP_BACK));
            fifthChildBoundaries.put(Boundary.LEFT_TOP_FRONT, this.boundaries.get(Boundary.LEFT_TOP_FRONT));
            fifthChildBoundaries.put(Boundary.RIGHT_TOP_FRONT, new CloudPoint(this.boundaries.get(Boundary.LEFT_TOP_FRONT).X() + xSplit, this.boundaries.get(Boundary.LEFT_TOP_FRONT).Y(), this.boundaries.get(Boundary.LEFT_TOP_FRONT).Z()));
            fifthChildBoundaries.put(Boundary.LEFT_TOP_BACK, new CloudPoint(this.boundaries.get(Boundary.LEFT_TOP_FRONT).X(), this.boundaries.get(Boundary.LEFT_TOP_FRONT).Y(), this.boundaries.get(Boundary.LEFT_TOP_FRONT).Z() + zSplit));
            fifthChildBoundaries.put(Boundary.RIGHT_TOP_BACK, new CloudPoint(this.boundaries.get(Boundary.LEFT_TOP_FRONT).X() + xSplit, this.boundaries.get(Boundary.LEFT_TOP_FRONT).Y(), this.boundaries.get(Boundary.LEFT_TOP_FRONT).Z() + zSplit));
            this.children.put(Children.FIFTH, new ParallelStreamCloud(this, fifthChildBoundaries));
            
            EnumMap<Boundary, CloudPoint> sixthChildBoundaries = new EnumMap<>(Boundary.class);
            sixthChildBoundaries.put(Boundary.LEFT_BOTTOM_FRONT, firstChildBoundaries.get(Boundary.RIGHT_TOP_FRONT));
            sixthChildBoundaries.put(Boundary.RIGHT_BOTTOM_FRONT, secondChildBoundaries.get(Boundary.RIGHT_TOP_FRONT));
            sixthChildBoundaries.put(Boundary.LEFT_BOTTOM_BACK, firstChildBoundaries.get(Boundary.RIGHT_TOP_BACK));
            sixthChildBoundaries.put(Boundary.RIGHT_BOTTOM_BACK, secondChildBoundaries.get(Boundary.RIGHT_TOP_BACK));
            sixthChildBoundaries.put(Boundary.LEFT_TOP_FRONT, fifthChildBoundaries.get(Boundary.RIGHT_TOP_FRONT));
            sixthChildBoundaries.put(Boundary.RIGHT_TOP_FRONT, this.boundaries.get(Boundary.RIGHT_TOP_FRONT));
            sixthChildBoundaries.put(Boundary.LEFT_TOP_BACK, fifthChildBoundaries.get(Boundary.RIGHT_TOP_BACK));
            sixthChildBoundaries.put(Boundary.RIGHT_TOP_BACK, new CloudPoint(this.boundaries.get(Boundary.RIGHT_TOP_FRONT).X(), this.boundaries.get(Boundary.RIGHT_TOP_FRONT).Y(), this.boundaries.get(Boundary.RIGHT_TOP_FRONT).Z() + zSplit));
            this.children.put(Children.SIXTH, new ParallelStreamCloud(this, sixthChildBoundaries));
            
            EnumMap<Boundary, CloudPoint> seventhChildBoundaries = new EnumMap<>(Boundary.class);
            seventhChildBoundaries.put(Boundary.LEFT_BOTTOM_FRONT, firstChildBoundaries.get(Boundary.LEFT_TOP_BACK));
            seventhChildBoundaries.put(Boundary.RIGHT_BOTTOM_FRONT, firstChildBoundaries.get(Boundary.RIGHT_TOP_BACK));
            seventhChildBoundaries.put(Boundary.LEFT_BOTTOM_BACK, thirdChildBoundaries.get(Boundary.LEFT_TOP_BACK));
            seventhChildBoundaries.put(Boundary.RIGHT_BOTTOM_BACK, thirdChildBoundaries.get(Boundary.RIGHT_TOP_BACK));
            seventhChildBoundaries.put(Boundary.LEFT_TOP_FRONT, fifthChildBoundaries.get(Boundary.LEFT_TOP_BACK));
            seventhChildBoundaries.put(Boundary.RIGHT_TOP_FRONT, fifthChildBoundaries.get(Boundary.RIGHT_TOP_BACK));
            seventhChildBoundaries.put(Boundary.LEFT_TOP_BACK, this.boundaries.get(Boundary.LEFT_TOP_BACK));
            seventhChildBoundaries.put(Boundary.RIGHT_TOP_BACK, new CloudPoint(this.boundaries.get(Boundary.LEFT_TOP_BACK).X() + xSplit, this.boundaries.get(Boundary.LEFT_TOP_BACK).Y(), this.boundaries.get(Boundary.LEFT_TOP_BACK).Z()));
            this.children.put(Children.SEVENTH, new ParallelStreamCloud(this, seventhChildBoundaries));
            
            EnumMap<Boundary, CloudPoint> eighthChildBoundaries = new EnumMap<>(Boundary.class);
            eighthChildBoundaries.put(Boundary.LEFT_BOTTOM_FRONT, firstChildBoundaries.get(Boundary.RIGHT_TOP_BACK));
            eighthChildBoundaries.put(Boundary.RIGHT_BOTTOM_FRONT, secondChildBoundaries.get(Boundary.RIGHT_TOP_BACK));
            eighthChildBoundaries.put(Boundary.LEFT_BOTTOM_BACK, thirdChildBoundaries.get(Boundary.RIGHT_TOP_BACK));
            eighthChildBoundaries.put(Boundary.RIGHT_BOTTOM_BACK, fourthChildBoundaries.get(Boundary.RIGHT_TOP_BACK));
            eighthChildBoundaries.put(Boundary.LEFT_TOP_FRONT, fifthChildBoundaries.get(Boundary.RIGHT_TOP_BACK));
            eighthChildBoundaries.put(Boundary.RIGHT_TOP_FRONT, sixthChildBoundaries.get(Boundary.RIGHT_TOP_BACK));
            eighthChildBoundaries.put(Boundary.LEFT_TOP_BACK, seventhChildBoundaries.get(Boundary.RIGHT_TOP_BACK));
            eighthChildBoundaries.put(Boundary.RIGHT_TOP_BACK, this.boundaries.get(Boundary.RIGHT_TOP_BACK));
            this.children.put(Children.EIGHTH, new ParallelStreamCloud(this, eighthChildBoundaries));
            
            // TODO test variants speed
            // Variant A
            
            this.points.parallelStream().forEach(point -> {
                if (pointBelongsToChild(point, Children.FIRST)) {
                    children.get(Children.FIRST).insertPoint(point);
                } else if (pointBelongsToChild(point, Children.SECOND)) {
                    children.get(Children.SECOND).insertPoint(point);
                } else if (pointBelongsToChild(point, Children.THIRD)) {
                    children.get(Children.THIRD).insertPoint(point);
                } else if (pointBelongsToChild(point, Children.FOURTH)) {
                    children.get(Children.FOURTH).insertPoint(point);
                } else if (pointBelongsToChild(point, Children.FIFTH)) {
                    children.get(Children.FIFTH).insertPoint(point);
                } else if (pointBelongsToChild(point, Children.SIXTH)) {
                    children.get(Children.SIXTH).insertPoint(point);
                } else if (pointBelongsToChild(point, Children.SEVENTH)) {
                    children.get(Children.SEVENTH).insertPoint(point);
                } else if (pointBelongsToChild(point, Children.EIGHTH)) {
                    children.get(Children.EIGHTH).insertPoint(point);
                } else {
                    System.out.println("Point does not belong to any child, debug please!");
                }
            });
            /*
            children.get(Children.FIRST).points = points.parallelStream().filter(point -> pointBelongsToChild(point, Children.FIRST)).collect(Collectors.toList());
            children.get(Children.SECOND).points =points.parallelStream().filter(point -> pointBelongsToChild(point, Children.SECOND)).collect(Collectors.toList());
            children.get(Children.THIRD).points =points.parallelStream().filter(point -> pointBelongsToChild(point, Children.THIRD)).collect(Collectors.toList());
            children.get(Children.FOURTH).points =points.parallelStream().filter(point -> pointBelongsToChild(point, Children.FOURTH)).collect(Collectors.toList());
            children.get(Children.FIFTH).points =points.parallelStream().filter(point -> pointBelongsToChild(point, Children.FIFTH)).collect(Collectors.toList());
            children.get(Children.SIXTH).points =points.parallelStream().filter(point -> pointBelongsToChild(point, Children.SIXTH)).collect(Collectors.toList());
            children.get(Children.SEVENTH).points =points.parallelStream().filter(point -> pointBelongsToChild(point, Children.SEVENTH)).collect(Collectors.toList());
            children.get(Children.EIGHTH).points = points.parallelStream().filter(point -> pointBelongsToChild(point, Children.EIGHTH)).collect(Collectors.toList());
            */
            this.points.clear();
        } else {
            this.children.get(Children.FIRST).split();
            this.children.get(Children.SECOND).split();
            this.children.get(Children.THIRD).split();
            this.children.get(Children.FOURTH).split();
            this.children.get(Children.FIFTH).split();
            this.children.get(Children.SIXTH).split();
            this.children.get(Children.SEVENTH).split();
            this.children.get(Children.EIGHTH).split();
        }
        
        return true;
    }
    
    public final boolean hasBoundariesCalculated() {
        return !this.boundaries.isEmpty();
    }
    
    public boolean hasChildren() {
        return !this.children.isEmpty();
    }
    
    public int size() {
        int count = 0;
        
        if (this.hasChildren()) {
            count += children.get(Children.FIRST).size();
            count += children.get(Children.SECOND).size();
            count += children.get(Children.THIRD).size();
            count += children.get(Children.FOURTH).size();
            count += children.get(Children.FIFTH).size();
            count += children.get(Children.SIXTH).size();
            count += children.get(Children.SEVENTH).size();
            count += children.get(Children.EIGHTH).size();
        } else {
            count += this.points.size();
        }
        
        return count;
    }
    
    public int countOfChildren() {
        int count = 0;
        
        if (this.hasChildren()) {
            count += 8;
            
            count += children.get(Children.FIRST).countOfChildren();
            count += children.get(Children.SECOND).countOfChildren();
            count += children.get(Children.THIRD).countOfChildren();
            count += children.get(Children.FOURTH).countOfChildren();
            count += children.get(Children.FIFTH).countOfChildren();
            count += children.get(Children.SIXTH).countOfChildren();
            count += children.get(Children.SEVENTH).countOfChildren();
            count += children.get(Children.EIGHTH).countOfChildren();
        }
        
        return count;
    }
    
    public boolean simplify() {
        if (this.children.isEmpty()) {
            if (this.points.size() > 0) {
                final AtomicFloat sumX = new AtomicFloat(0.0f);
                final AtomicFloat sumY = new AtomicFloat(0.0f);
                final AtomicFloat sumZ = new AtomicFloat(0.0f);
                
                double avX = this.points.parallelStream().mapToDouble(p -> p.X()).average().getAsDouble();
                double avY = this.points.parallelStream().mapToDouble(p -> p.Y()).average().getAsDouble();
                double avZ = this.points.parallelStream().mapToDouble(p -> p.Z()).average().getAsDouble();
                
                CloudPoint centerOfMass = new CloudPoint((float)avX, (float)avY, (float)avZ);
                
                this.points.clear();
                this.points.add(centerOfMass);
            }
        } else {
            children.get(Children.FIRST).simplify();
            children.get(Children.SECOND).simplify();
            children.get(Children.THIRD).simplify();
            children.get(Children.FOURTH).simplify();
            children.get(Children.FIFTH).simplify();
            children.get(Children.SIXTH).simplify();
            children.get(Children.SEVENTH).simplify();
            children.get(Children.EIGHTH).simplify();
        }
        
        return true;
    }
    
    public String cloudInfo() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Points count: ");
        buffer.append(this.size());
        buffer.append("\nChildren count: ").append(this.countOfChildren());
        return buffer.toString();
    }
}
