/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paraclouds.CloudEngine;

public class CloudPoint {
    private float positionX;
    private float positionY;
    private float positionZ;
    
    public CloudPoint(float x, float y, float z) {
        this.positionX = x;
        this.positionY = y;
        this.positionZ = z;
    }
    
    public CloudPoint() {
        this.positionX = 0.0f;
        this.positionY = 0.0f;
        this.positionZ = 0.0f;
    }
    
    public float X() {
        return this.positionX;
    }
    
    public float Y() {
        return this.positionY;
    }
    
    public float Z() {
        return this.positionZ;
    }
    
    public void setX(float newX) {
        this.positionX = newX;
    }
    
    public void setY(float newY) {
        this.positionY = newY;
    }
    
    public void setZ(float newZ) {
        this.positionZ = newZ;
    }
    
    public boolean isSameAs(CloudPoint point) {
        return (this.X() == point.X() && this.Y() == point.Y() && this.Z() == point.Z());
    }
}
