/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paraclouds.CloudEngine;

/**
 *
 * @author dan
 */
public interface Simplifiable {
    public boolean simplify();
    public boolean split();
}
